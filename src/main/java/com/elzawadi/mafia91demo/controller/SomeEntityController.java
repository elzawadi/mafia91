package com.elzawadi.mafia91demo.controller;

import com.elzawadi.mafia91demo.model.SomeEntity;
import com.elzawadi.mafia91demo.repository.SomeEntityRepository;
import com.elzawadi.mafia91demo.service.SomeEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/someEntity")
public class SomeEntityController {

    @Autowired
    private SomeEntityService someEntityService;

    @GetMapping(path = "/create")
    public @ResponseBody String createSomeEntity(@RequestParam Integer value) {
        someEntityService.create(value);
        return "created";
    }

    @GetMapping(path = "/increment")
    public @ResponseBody String increment(@RequestParam Integer id, @RequestParam Integer value) {
        someEntityService.incrementValue(id, value);
        return "incremented";
    }

}
