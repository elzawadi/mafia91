package com.elzawadi.mafia91demo.service;

import com.elzawadi.mafia91demo.model.SomeEntity;
import com.elzawadi.mafia91demo.repository.SomeEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class SomeEntityServiceImpl implements SomeEntityService {

    @Autowired
    private SomeEntityRepository someEntityRepository;

    @Override
    public void create(Integer value) {
        SomeEntity someEntity = new SomeEntity();
        someEntity.setValue(0);
        someEntityRepository.save(someEntity);
    }

    @Override
    @Transactional
    public void incrementValue(Integer someEntityId, Integer value) {
        Optional<SomeEntity> someEntity = someEntityRepository.findById(1);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            System.out.println("someValue fetched from database");
        }
        someEntity.get().setValue(someEntity.get().getValue()+value);
        someEntityRepository.save(someEntity.get());
    }
}
