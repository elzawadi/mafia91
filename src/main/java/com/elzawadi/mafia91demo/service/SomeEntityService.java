package com.elzawadi.mafia91demo.service;

public interface SomeEntityService {

    void create(Integer value);

    void incrementValue(Integer someEntityId, Integer value);
}
