package com.elzawadi.mafia91demo.repository;

import com.elzawadi.mafia91demo.model.SomeEntity;
import org.springframework.data.repository.CrudRepository;

public interface SomeEntityRepository extends CrudRepository<SomeEntity, Integer> {
}
