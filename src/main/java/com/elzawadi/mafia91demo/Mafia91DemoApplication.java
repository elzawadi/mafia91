package com.elzawadi.mafia91demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mafia91DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Mafia91DemoApplication.class, args);
	}
}
